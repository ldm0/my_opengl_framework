#define _DRAW_ELEMENT

#include<glad/glad.h>
#include<GLFW/glfw3.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"window.h"
#include"shader.h"
#define STB_IMAGE_IMPLEMENTATION
#include<stb_image.h>

#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>

#define MAIN_WINDOW_WIDTH	1024
#define MAIN_WINDOW_HEIGHT	768

GLfloat vertices[] = {
	-0.5f,-0.5f,0.0f,0.f,0.f,
	-0.5f,0.5f,0.0f,0.f,1.f,
	0.5f,-0.5f,0.0f,1.f,0.f,
	0.5f,0.5f,0.0f,1.f,1.f,
};

GLuint indices[] = {
	0,1,2,
	1,2,3,
};

//FragColor=0.5*(vec4(pos.x+sin,pos.y+sin,pos.z+sin,1.0f)+texture(img,tex_coord_fg));
//void frameBuffer_resize_callback(GLFWwindow* window, int width, int height);

void key_process(GLFWwindow* window);

void bgcolor_set(GLfloat r, GLfloat g, GLfloat b, GLfloat a);

int
main()
{
#pragma region init environment
	glfwInit();

	/*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/
	glfwWindowHint(GLFW_REFRESH_RATE, 60);

	Window main_window(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT, "fuck you");
/*
	GLFWwindow *window_main = glfwCreateWindow(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT, "main window", NULL, NULL);
	if (!window_main) {
		printf("[error]\t main window can't create!\n\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	printf("[success]\t main window created\n\n");
*/
	main_window.context_current();
	//glfwMakeContextCurrent(window_main);

	if (!gladLoadGL()) {
		printf("[error]\tglad can't load!\n\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	printf("[success]\tglad initialized\n\n");

	//should be placed after gladLoadGL
	printf("[GL version]\t%s\n\n", glGetString(GL_VERSION));

	//see the num_attr
	int num_attr;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &num_attr);
	printf("[max numAttr]\t%d\n\n", num_attr);

	//glViewport(0, 0, MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
	//glfwSetFramebufferSizeCallback(window_main, &frameBuffer_resize_callback);
	//glfwSetFramebufferSizeCallback(window_main, NULL);
#pragma endregion


#pragma region create vertex array

	GLuint varray;
	glCreateVertexArrays(1, &varray);
	if (!varray) {
		printf("[error]\tcant create vertex array!\n\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	printf("[success]\tvertex array created\n\n");
#pragma endregion


#pragma region create vertex buffer
	GLuint vbuffer = 0;
	glCreateBuffers(1, &vbuffer);
	if (!vbuffer) {
		printf("[error]\tcant create vertex buffer!\n\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	printf("[success]\tvertex buffer created\n\n");
#pragma endregion


#pragma region load img

	GLint img_width, img_height, nr_channel;
	//forced to be "\\"
	GLubyte *img = stbi_load("res\\timg.jpg", &img_width, &img_height, &nr_channel,0);
	if (!img) {
		printf("[error]\tcant load img, check the path!\n\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	printf("[success]\timg loaded\n\n");

#pragma endregion


#pragma region create texture

	GLuint tex;
	//glCreateTextures(GL_TEXTURE_2D, 1, &tex);
	glGenTextures(1, &tex);

#pragma endregion

#pragma region create element buffer

	GLuint ebuffer;
	glCreateBuffers(1, &ebuffer);
	if (!ebuffer) {
		printf("[error]\tcant create element buffer!\n\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	printf("[success]\telement buffer created\n\n");
#pragma endregion


#pragma region init shaders 

	Shader vt_shader("./src/vt_shader.glsl");
	Shader fg_shader("./src/fg_shader.glsl");
	GLint b_compile_success;
	GLint b_link_success;
	char info[1024];

#pragma region init vertex shader
	GLuint shader_vt;
	shader_vt = glCreateShader(GL_VERTEX_SHADER);
	const char *c_vt_shader = vt_shader.c_str();
	glShaderSource(shader_vt, 1, &c_vt_shader, NULL);
	glCompileShader(shader_vt);
	glGetShaderiv(shader_vt, GL_COMPILE_STATUS, &b_compile_success);
	if (!b_compile_success) {
		printf("[error]\tcompile vertex shader unsuccessfully!\n\n");
		glGetShaderInfoLog(shader_vt, sizeof(info), NULL, info);
		printf(
			"Info############################\n"
			"%s\n"
			"################################\n\n", info);
		getchar();
		glfwTerminate();
		return -1;
	}
	printf("[success]\tvertex shader compiled\n\n");
#pragma endregion

#pragma region init fragment shader

	GLuint shader_fg;
	shader_fg = glCreateShader(GL_FRAGMENT_SHADER);
	const char *c_fg_shader = fg_shader.c_str();
	glShaderSource(shader_fg, 1, &c_fg_shader, NULL);
	glCompileShader(shader_fg);

	glGetShaderiv(shader_fg, GL_COMPILE_STATUS, &b_compile_success);
	if (!b_compile_success) {
		printf("[error]\tcompile fragment shader unsuccessfully!\n\n");
		glGetShaderInfoLog(shader_fg, sizeof(info), NULL, info);
		printf(
			"Info############################\n"
			"%s\n"
			"################################\n\n", info);
		getchar();
		glfwTerminate();
		return -1;
	}

	printf("[success]\tfragment shader compiled\n\n");


#pragma endregion

#pragma endregion


#pragma region init shader program

	GLuint shader_program;
	shader_program = glCreateProgram();
	glAttachShader(shader_program, shader_vt);
	glAttachShader(shader_program, shader_fg);
	glLinkProgram(shader_program);
	glGetProgramiv(shader_program, GL_LINK_STATUS, &b_link_success);
	if (!b_link_success) {
		printf("[error]\tshader link error\n\n");
		glGetProgramInfoLog(shader_program, sizeof(info), NULL, info);
		printf(
			"Info############################\n"
			"%s\n"
			"################################\n\n", info);
		getchar();
		glfwTerminate();
		return -1;
	}
	printf("[success]\tshader program linked\n\n");

#pragma endregion

	//After program initialized, the shaders are useless
	glDeleteShader(shader_vt);
	glDeleteShader(shader_fg);

#pragma region configure

	glUseProgram(shader_program);

	glBindVertexArray(varray);
	glBindBuffer(GL_ARRAY_BUFFER, vbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img_width, img_height, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(glGetAttribLocation(shader_program, "pos_init"), 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);
	glEnableVertexAttribArray(glGetAttribLocation(shader_program, "pos_init"));

	glVertexAttribPointer(glGetAttribLocation(shader_program, "tex_coord"), 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(glGetAttribLocation(shader_program, "tex_coord"));

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
#pragma endregion

	stbi_image_free(img);

#pragma region main loop
	GLfloat position_z = -3.0f, position_y = 0.f, position_x = 0.f;
	//while (!glfwWindowShouldClose(window_main)) {
	while (!main_window.should_close()) {

		//key_process(window_main);
		if (main_window.get_key(GLFW_KEY_ESCAPE) == GLFW_PRESS)
			break;
		if (main_window.get_key(GLFW_KEY_W) == GLFW_PRESS)
			position_z += 0.005f;
		if (main_window.get_key(GLFW_KEY_S) == GLFW_PRESS)
			position_z -= 0.005f;
		if (main_window.get_key(GLFW_KEY_D) == GLFW_PRESS)
			position_x += 0.005f;
		if (main_window.get_key(GLFW_KEY_A) == GLFW_PRESS)
			position_x -= 0.005f;
		if (main_window.get_key(GLFW_KEY_SPACE) == GLFW_PRESS)
			position_y += 0.005f;
		if (main_window.get_key(GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
			position_y -= 0.005f;
		//glfwGetCursorPos(window_main,)

		const glm::mat4 identy = glm::mat4(1.f);
		glm::mat4 trans= glm::mat4(1.0f);
		trans = glm::rotate(identy, (GLfloat)glm::radians(fmod(glfwGetTime()*100,360.f)), glm::vec3(0.5f, 0.5f, 0.5f));
		trans = glm::translate(identy, glm::vec3(position_x, position_y, position_z)) * trans;
		trans = glm::perspective(30.f, MAIN_WINDOW_WIDTH / (GLfloat)MAIN_WINDOW_HEIGHT, .1f, 100.f) * trans;
		glUniformMatrix4fv(glGetUniformLocation(shader_program, "trans"),1,GL_FALSE,glm::value_ptr(trans));

		GLdouble sin_val = sin(fmod(glfwGetTime(),2*3.1416));
		glUniform1f(glGetUniformLocation(shader_program, "sin"),(GLfloat)sin_val);

		GLdouble time= glfwGetTime();
		glUniform1f(glGetUniformLocation(shader_program, "time"),(GLfloat)time);

		bgcolor_set(1.0f, 1.0f, 1.0f, 1.0f);

#ifdef _DRAW_ELEMENT
		glDrawElements(GL_TRIANGLES, sizeof(indices)/sizeof(indices[0]), GL_UNSIGNED_INT,0);
#else
		glDrawArrays(GL_TRIANGLES, 0, 3);
#endif // _DRAW_ELEMENT

		//glfwSwapBuffers(window_main);
		main_window.buffer_swap();
		glfwPollEvents();
	}
#pragma endregion

	glfwTerminate();
	return 0;
}

//void
//frameBuffer_resize_callback(GLFWwindow* window, int width, int height)
//{
//	glViewport(0, 0, width, height);
//}

void
bgcolor_set(GLfloat r, GLfloat g, GLfloat b, GLfloat a)
{
	glClearColor(r, g, b, a);
	glClear(GL_COLOR_BUFFER_BIT);
}

void
key_process(GLFWwindow* window)
{

}
