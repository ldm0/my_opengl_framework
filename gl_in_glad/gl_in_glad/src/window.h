#pragma once

#include<GLFW/glfw3.h>

void resize_callback(GLFWwindow *window, int width, int height);

class Window {
	int _width;
	int _height;
	GLFWwindow *_window;
public:
	Window(int width, int height, const char *title, GLFWframebuffersizefun fun = resize_callback);
	Window(const Window &window);
	~Window();
	void context_current(void);
	int should_close(void);
	int get_key(int key);
	void change_size(int width, int height);
	void buffer_swap(void);
};

