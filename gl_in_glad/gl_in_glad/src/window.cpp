#include "window.h"

void resize_callback(GLFWwindow *window, int width, int height)
{
	glViewport(0, 0, width, height);
}

Window::Window(int width, int height, const char *title, GLFWframebuffersizefun fun)
	: _width(width), _height(height)
{
	_window = glfwCreateWindow(width, height, title, NULL/*glfwGetPrimaryMonitor()*/, NULL);
	if (!_window)
		__debugbreak();
	glfwSetFramebufferSizeCallback(_window, fun);
	glViewport(0, 0, width, height);
}

Window::Window(const Window &window)
{
	_width = window._width;
	_height = window._height;
	//Don't copy the title
	_window = glfwCreateWindow(_width, _height, "window_copy", NULL, NULL);
}

Window::~Window()
{
	glfwDestroyWindow(_window);
}

void Window::context_current(void)
{
	glfwMakeContextCurrent(_window);
}

int Window::should_close(void)
{
	return glfwWindowShouldClose(_window);
}

int Window::get_key(int key)
{
	return glfwGetKey(_window, key);
}

void Window::change_size(int width, int height)
{
	_width = width;
	_height = height;
}

void Window::buffer_swap(void)
{
	glfwSwapBuffers(_window);
}

