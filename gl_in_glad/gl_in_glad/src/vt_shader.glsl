#version 450 core
in vec3 pos_init;
in vec2 tex_coord;
out vec3 pos;
out vec2 tex_coord_fg;
uniform float sin;
uniform mat4 trans;

void main(){
	gl_Position=trans*vec4(pos_init,1.0f);
	pos=pos_init.xyz*2;
	tex_coord_fg=tex_coord;
}