#include "shader.h"

Shader::Shader(const std::string path)
{
	if (!read_file(path))
		__debugbreak();
}

Shader::Shader(Shader &shader)
{
	shader_str = shader.shader_str;
}

Shader::~Shader()
{
}

const char* Shader::c_str(void)
{
	return shader_str.c_str();
}

int Shader::read_file(const std::string &path)
{
	std::ifstream _file(path);
	shader_str.assign((std::istreambuf_iterator<char> (_file)), (std::istreambuf_iterator<char> ()));
	return true;
}
