#pragma once

#include<istream>
#include<fstream>
#include<string>

class Shader {
	std::string shader_str;
public:
	Shader(const std::string path);
	Shader(Shader &shader);
	~Shader();
	const char* c_str(void);
	int read_file(const std::string &path);
};

