#version 450 core
in vec3 pos;
in vec2 tex_coord_fg;
out vec4 FragColor;
uniform float sin;
uniform float time;
uniform sampler2D img;
void main()
{
	FragColor = 0.1*vec4(pos.x + sin, pos.y + sin, pos.z + sin, 1.0f)
		+
		0.9*texture(
			img,
			tex_coord_fg + 0.01*sin(
				vec2(
					mix(
						tex_coord_fg.x,
						tex_coord_fg.y,
						0.7
					) * 40 + time * 17 + 7 * sin,
					mix(
						tex_coord_fg.y,
						tex_coord_fg.x,
						0.7
					) * 50 + time * 14 + 4 * sin
				)
			)
			);
}