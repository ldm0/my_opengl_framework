#include "renderer.h"

void file2str(const std::string file_path, std::string &destination)
{
	std::ifstream file(file_path);
	destination.assign((std::istreambuf_iterator<char> (file)), (std::istreambuf_iterator<char> ()));
}