#pragma once

#define GLEW_STATIC
#include<GL/glew.h>

class Id_buffer {
private:
	GLuint m_renderer_id;
	GLuint m_count;
public:
	Id_buffer(const GLuint *data, GLuint count);
	~Id_buffer();

	void bind()const;
	void unbind()const;
};