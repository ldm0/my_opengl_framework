#include<iostream>
#include<fstream>
#include<string>
#include<ctime>
#include"renderer.h"
#include"vt_buffer.h"
#include"id_buffer.h"
#define GLEW_STATIC
#include<GL/glew.h>
#include<GLFW/glfw3.h>

#define RESOLUTION_X 1000
#define RESOLUTION_Y 600


static GLuint shader_create(int shader_type, const std::string &shader_source)
{
	GLuint _shader = glCreateShader(shader_type);
	const char *_shader_source = shader_source.c_str();
	glShaderSource(_shader, 1, &_shader_source, nullptr);
	glCompileShader(_shader);
	int _compile_status;
	glGetShaderiv(_shader, GL_COMPILE_STATUS, &_compile_status);
	ASSERT(_compile_status == GL_TRUE);
	return _shader;
}

static GLuint program_create(const std::string &vt_shader, const std::string &fg_shader)
{
	GLuint _program = glCreateProgram();
	GLuint _vt_shader = shader_create(GL_VERTEX_SHADER, vt_shader);
	GLuint _fg_shader = shader_create(GL_FRAGMENT_SHADER, fg_shader);
	glAttachShader(_program, _vt_shader);
	glAttachShader(_program, _fg_shader);
	glLinkProgram(_program);
	glValidateProgram(_program);
	int _validate_status;
	glGetProgramiv(_program, GL_VALIDATE_STATUS, &_validate_status);
	ASSERT(_validate_status == GL_TRUE);
	glDeleteShader(_vt_shader);
	glDeleteShader(_fg_shader);
	return _program;
}

int main()
{
	ASSERT(glfwInit() == GLFW_TRUE);

	GLFWwindow *main_window = glfwCreateWindow(800, 600, "main_window", NULL, NULL);
	ASSERT(main_window);

	glfwMakeContextCurrent(main_window);

	ASSERT(glewInit() == GLEW_OK);

	float positions[] = {
		-1.f, -1.f,
		1.f, -1.f,
		1.f, 1.f,
		-1.f, 1.f,
	};

	GLuint indices[] = {
		0, 1, 2,
		2, 3, 0, 
	};

	/*GLuint _vt_array;
	glGenVertexArrays(1, &_vt_array);
	glBindVertexArray(_vt_array);*/

	//Vt_buffer _vt_buffer(positions, sizeof(positions));
	//Id_buffer _id_buffer(indices, sizeof(indices)/sizeof(indices[0]));

	///*
	GLuint _vt_buffer;
	glGenBuffers(1, &_vt_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, _vt_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(positions), positions, GL_STATIC_DRAW);

	GLuint _id_buffer;
	glGenBuffers(1, &_id_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _id_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
	//*/
	
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);

	std::string _vt_shader;
	file2str("vt_shader.glsl", _vt_shader);
	std::string _fg_shader;
	file2str("fg_shader_fractal.glsl", _fg_shader);

	GLuint _program = program_create(_vt_shader, _fg_shader);
	glUseProgram(_program);

	int location_time = glGetUniformLocation(_program, "iTime");
	ASSERT(location_time >= 0);
	int location_resolution = glGetUniformLocation(_program, "iResolution");
	ASSERT(location_resolution >= 0);
	glUniform2i(location_resolution, RESOLUTION_X, RESOLUTION_Y);

	//clock_t time_start = clock();
	clock_t time_start = 0;

	while (!glfwWindowShouldClose(main_window)) {
		glClear(GL_COLOR_BUFFER_BIT);

		float time_during = (clock() - time_start) / (float)1000.;

		printf("%f\n", time_during);

		glUniform1f(location_time, time_during);

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

		glfwSwapBuffers(main_window);

		glfwPollEvents();
	}

	glDeleteProgram(_program);

	glfwTerminate();

	return 0;
}

