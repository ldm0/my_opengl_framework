#pragma once

#define GLEW_STATIC
#include<GL/glew.h>

class Vt_buffer {
private:
	GLuint m_renderer_id;
public:
	Vt_buffer(const void *data, size_t size);
	~Vt_buffer();

	void bind()const;
	void unbind()const;
};