#include "vt_buffer.h"

Vt_buffer::Vt_buffer(const void *data, size_t size)
{
	//Binded form start
	glGenBuffers(1, &m_renderer_id);
	glBindBuffer(GL_ARRAY_BUFFER, m_renderer_id);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}

Vt_buffer::~Vt_buffer()
{
	glDeleteBuffers(1, &m_renderer_id);
}

void Vt_buffer::bind()const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_renderer_id);
}

void Vt_buffer::unbind()const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
