#include "id_buffer.h"

Id_buffer::Id_buffer(const GLuint *data, GLuint count)
	:m_count(count)
{
	//Binded from start
	glGenBuffers(1, &m_renderer_id);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_renderer_id);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(GLuint), data, GL_STATIC_DRAW);
}

Id_buffer::~Id_buffer()
{
	glDeleteBuffers(1, &m_renderer_id);
}

void Id_buffer::bind()const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_renderer_id);
}

void Id_buffer::unbind()const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
