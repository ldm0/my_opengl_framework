#pragma once

#include<iostream>
#include<fstream>
#include<string>

#define ASSERT(n) if (!(n)) __debugbreak()

void file2str(const std::string file_path, std::string &destination);